const joi = require("@hapi/joi");


function validateAddTask(task) {
    let schema = joi.object({
        TaskName: joi.string().required(),
        Description: joi.string().required(),
        projectName: joi.string().required(),
        status: joi.string().required(),
    })

    let result = schema.validate(task)
    return result
}

function validateUpdateTasks(task) {
    let schema = joi.object({
        TaskName: joi.string(),
        Description: joi.string(),
        projectName: joi.string(),
        status: joi.string()
    })

    let result = schema.validate(task)
    return result
}

module.exports.validateAddTask = validateAddTask
module.exports.validateUpdateTasks = validateUpdateTasks