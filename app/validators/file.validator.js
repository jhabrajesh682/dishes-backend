const joi = require("@hapi/joi");


function validateAddFile(file) {
    let schema = joi.object({
        Name: joi.string().required(),
        photo: joi.string(),
        orignalName: joi.string(),
    })

    let result = schema.validate(file)
    return result
}

module.exports.validateAddFile = validateAddFile;